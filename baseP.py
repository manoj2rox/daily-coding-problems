


def modulo_div(n, original_base, destination_base):
    carry = 0
    for i in range(len(n)):
        d = n[i]
        d += original_base*carry
        carry = d % destination_base
        d = (d//destination_base)
        n[i] = d
        #print(i,d,carry)
    return carry


def is_zero(n):
    for d in n:
        if d != 0:
            return False
    return True


def convertBase(n, original_base, destination_base):
    digits = []
    while not is_zero(n):
        digits.insert(0, modulo_div(n, original_base, destination_base))
    return digits


n = [int(i) for i in input()]

# print("".join(convertBase(n, 2, 6)))
# print(convertBase(n,2,6))
print("".join([str(elem) for elem in convertBase(n,2,6)]))
